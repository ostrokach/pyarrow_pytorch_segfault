# PyArrow / PyTorch Segfault

## Failing example

```bash
$ ./test_fail.sh 
1
2
3
4
Fatal Python error: Segmentation fault

Current thread 0x00007f52959bb740 (most recent call first):
  File "/home/kimlab1/strokach/anaconda/lib/python3.6/site-packages/pyarrow/parquet.py", line 125 in read_row_group
  File "<string>", line 1 in <module>
./test_fail.sh: line 5: 42612 Segmentation fault      (core dumped) python -X faulthandler -c "import torch; import pyarrow.parquet as pq; _ = pq.ParquetFile('example.parquet').read_row_group(0)"
```

## Passing example

```bash
$ ./test_pass.sh
1
2
3
4
5
6
7
8
9
10
```

