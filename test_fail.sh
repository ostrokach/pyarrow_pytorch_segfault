#!/bin/bash

set -e

for i in {1..100} ; do 
    echo $i
    python -X faulthandler -c "import torch; import pyarrow.parquet as pq; _ = pq.ParquetFile('example.parquet').read_row_group(0)"
done

